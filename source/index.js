let item = {
	itemType: null,
	item_name: null,
	//10 keys max
}

let pers = {
	level: 10,
	char_class: 'Frontend developer',
	conditions: {
		//5 slots
		max_life: 100,
		current_life: 50,
		max_stamina: 120,
		cur_stamina: 100,
	},
	stats: {
		//5 slots
		dexterity: 18,
		strength: 10,
		wisdom: 0,
		intellect: -5,
	},
	inventory: {
		//7 slots max
		head: {
			itemType: 'helmet',
			itemName: 'bucket',
			durability: 25,
			defense: 19
		},
		leftHand: {

		},
		rightHand: {

		},
		backpack: [{
			itemType: 'helmet',
			itemName: 'great helmet of fullstack developer',
			durability: 25,
			durability: 300,
			defense: 156
		},]
	},
}
function getItem(name,) {
	//body of func
	let newItem = Object.assign({}, item);
	newItem.item_name = 'Normal sword';
	newItem.durability = 30;

	//some more
	pers.backpack.push(newItem)
}

function checkCaptain(name, hasShip, callback) {
	//your code
}
const captainizeIt = (string, hasShip) => {
	//your code

}
checkCaptain('Jack Sparrow', true, myAwesomeFunc)
checkCaptain('Jack Sparrow', false, myAwesomeFunc)

function checkCaptain(name, hasShip, callback) {
	let modified = name + ' is a pirate'
	callback(modified, hasShip)
}
const myAwesomeFunc = (string, hasShip) => {
	if (hasShip) {
		console.log(string + '. And a captain also')
	} else {
		console.log(string)
	}

}
checkCaptain('Jack Sparrow', true, myAwesomeFunc)